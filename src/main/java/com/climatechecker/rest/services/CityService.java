package com.climatechecker.rest.services;

import com.climatechecker.domains.City;
import com.climatechecker.rest.repositories.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public List<City> getAll() { return cityRepository.findAll(); }
    public Page<City> findByName(String name, Pageable pageable) { return cityRepository.findByName(name, pageable); }

    public void save(City city) {
        cityRepository.save(city);
    }

}
