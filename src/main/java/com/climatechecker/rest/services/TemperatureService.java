package com.climatechecker.rest.services;

import com.climatechecker.domains.Temperature;
import com.climatechecker.rest.repositories.TemperatureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TemperatureService {

    @Autowired
    private TemperatureRepository temperatureRepository;

    public List<Temperature> getAll() { return temperatureRepository.findAll(); }

    public void save(Temperature temperature) {
        temperatureRepository.save(temperature);
    }

    public List<Temperature> findByDate(LocalDate date) {
        return temperatureRepository.findByTempdate(date);
    }

    public Temperature findLatest(){
        return temperatureRepository.findTopByOrderByTempdate();
    }
}
