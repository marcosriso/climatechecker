package com.climatechecker.rest;

import com.climatechecker.domains.City;
import com.climatechecker.domains.Temperature;
import com.climatechecker.rest.services.CityService;
import com.climatechecker.rest.services.TemperatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CityController {

    @Autowired
    CityService cityService;

    @GetMapping("/cities")
    public List<City> cityList() {
        return cityService.getAll();
    }

    @PostMapping(value = "/city", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void add(final @RequestBody City city){
        cityService.save(city);
    }
}
