package com.climatechecker.rest.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;

/**
 * Created by @mriso79 on 19/07/18.
 */
public abstract class AbstractDeserializer<E> extends JsonDeserializer<E> {

    protected abstract E deserialize(final JsonNode jsonNode) throws IOException;

    @Override
    public E deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException {

        final ObjectCodec objectCoded = jsonParser.getCodec();
        final JsonNode jsonNode = objectCoded.readTree(jsonParser);

        return this.deserialize(jsonNode);
    }

    protected String escapeJavaScript(String jsonvalue){
        Gson gson = new Gson();
        java.lang.String g = gson.toJson(jsonvalue);
        return StringEscapeUtils.escapeJavaScript(g);
    }
}
