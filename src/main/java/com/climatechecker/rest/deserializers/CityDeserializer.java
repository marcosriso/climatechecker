package com.climatechecker.rest.deserializers;

import com.climatechecker.domains.City;
import com.fasterxml.jackson.databind.JsonNode;

public class CityDeserializer extends AbstractDeserializer<City> {

    @Override
    public City deserialize(final JsonNode node) {

        final String name   = node.get("name").asText("");
        final String latlng = node.get("latlng").asText("");
        final Long openweatherid = node.get("openweatherid").asLong();

        City city = new City();

        city.setName(name);
        city.setLatlng(latlng);
        city.setOpenweatherid(openweatherid);

        return city;
    }

}
