package com.climatechecker.rest.deserializers;

import com.climatechecker.domains.Temperature;
import com.fasterxml.jackson.databind.JsonNode;

import java.time.LocalDate;

public class TemperatureDeserializer extends AbstractDeserializer<Temperature> {

    public Temperature deserialize(final JsonNode node) {

        final Double min_celsius      = node.get("min_celsius").asDouble();
        final Double max_celsius      = node.get("max_celsius").asDouble();
        final Double min_fahrenheit   = node.get("min_fahrenheit").asDouble();
        final Double max_fahrenheit   = node.get("max_fahrenheit").asDouble();
        final Double humidity         = node.get("humidity").asDouble();
        final String weather          = node.get("weather").asText();
        final LocalDate tempdate      = LocalDate.parse(node.get("temp_date").asText());
        final Long city_id            = node.get("city_id").asLong();

        Temperature temperature = new Temperature();

        temperature.setMin_celsius(min_celsius);
        temperature.setMax_celsius(max_celsius);
        temperature.setMin_fahrenheit(min_fahrenheit);
        temperature.setMax_fahrenheit(max_fahrenheit);
        temperature.setTempdate(tempdate);
        temperature.setCity_id(city_id);
        temperature.setHumidity(humidity);
        temperature.setWeather(weather);

        return temperature;
    }
}
