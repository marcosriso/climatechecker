package com.climatechecker.rest.deserializers;

import com.climatechecker.domains.TempCityQuery;
import com.climatechecker.domains.Temperature;
import com.fasterxml.jackson.databind.JsonNode;

import java.time.LocalDate;

public class TempCityQueryDeserializer extends AbstractDeserializer<TempCityQuery> {

    public TempCityQuery deserialize(final JsonNode node) {

        final Long cityId          = node.get("city_id").asLong();
        final LocalDate searchdate = LocalDate.parse(node.get("searchdate").asText());

        return new TempCityQuery(cityId, searchdate);
    }
}