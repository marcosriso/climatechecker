package com.climatechecker.rest.repositories;

import com.climatechecker.domains.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CityRepository extends PagingAndSortingRepository<City, Long> {
    List<City> findAll();
    Page<City> findAll(Pageable pageable);
    Page<City> findByName(String name, Pageable pageable);
}
