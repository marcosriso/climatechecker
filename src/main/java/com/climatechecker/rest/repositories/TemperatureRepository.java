package com.climatechecker.rest.repositories;

import com.climatechecker.domains.Temperature;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface TemperatureRepository extends PagingAndSortingRepository<Temperature, Long> {
    List<Temperature> findAll();
    Page<Temperature> findAll(Pageable pageable);
    List<Temperature> findByTempdate(LocalDate temp_date);
    Temperature findTopByOrderByTempdate();
}
