package com.climatechecker.rest;

import com.climatechecker.domains.CitySummary;
import com.climatechecker.domains.TempCityQuery;
import com.climatechecker.domains.Temperature;
import com.climatechecker.rest.services.TemperatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class TemperaturesController {

    @Autowired
    TemperatureService temperatureService;

    @GetMapping("/temperatures")
    public List<Temperature> TemperaturesList() {
         return temperatureService.getAll();
    }

    @GetMapping("/todaydate")
    public LocalDate todaydate() {
        return LocalDate.now();
    }

    @GetMapping("/latest")
    public Temperature latest() {
        return temperatureService.findLatest();
    }

    @PostMapping(value = "/temperature", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void add(final @RequestBody Temperature temp){
        temperatureService.save(temp);
    }

    @PostMapping(value = "/citysummary", consumes = MediaType.APPLICATION_JSON_VALUE)
    public CitySummary citysummary(final @RequestBody TempCityQuery tempCityQuery) {

       LocalDate querieddate     = tempCityQuery.getSearchdate();
       LocalDate oneyeardate     = querieddate.minusYears(1L);
       LocalDate fiveyearsdate   = querieddate.minusYears(5L);
       LocalDate tenyearsdate    = querieddate.minusYears(10L);
       LocalDate twentyyearsdate = querieddate.minusYears(20L);
       LocalDate fortyyearsdate  = querieddate.minusYears(40L);

       List<Temperature> listTemperature   = temperatureService.findByDate(querieddate);
       List<Temperature> listTemperature1  = temperatureService.findByDate(oneyeardate);
       List<Temperature> listTemperature5  = temperatureService.findByDate(fiveyearsdate);
       List<Temperature> listTemperature10 = temperatureService.findByDate(tenyearsdate);
       List<Temperature> listTemperature20 = temperatureService.findByDate(twentyyearsdate);
       List<Temperature> listTemperature40 = temperatureService.findByDate(fortyyearsdate);

       CitySummary citysummary = new CitySummary();

       listTemperature.forEach(temperature -> {
           citysummary.setTodayMin(temperature.getMin_celsius());
           citysummary.setTodayMax(temperature.getMax_celsius());
       });

        listTemperature1.forEach(temperature -> {
            citysummary.setOneyearMin(temperature.getMin_celsius());
            citysummary.setOneyearMax(temperature.getMax_celsius());
        });

        listTemperature5.forEach(temperature -> {
            citysummary.setFiveyearsMin(temperature.getMin_celsius());
            citysummary.setFiveyearsMax(temperature.getMax_celsius());
        });

        listTemperature10.forEach(temperature -> {
            citysummary.setTenyearsMin(temperature.getMin_celsius());
            citysummary.setTenyearsMax(temperature.getMax_celsius());
        });

        listTemperature20.forEach(temperature -> {
            citysummary.setTwentyyearsMin(temperature.getMin_celsius());
            citysummary.setTwentyyearsMax(temperature.getMax_celsius());
        });

        listTemperature40.forEach(temperature -> {
            citysummary.setFortyyearsMin(temperature.getMin_celsius());
            citysummary.setFortyyearsMax(temperature.getMax_celsius());
        });

       return citysummary;
    }

    @PostMapping(value = "/temperaturebydate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<Temperature> getTempByDate(final @RequestBody TempCityQuery tempCityQuery) {
        return temperatureService.findByDate(tempCityQuery.getSearchdate());
    }

}
