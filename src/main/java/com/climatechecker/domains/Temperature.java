package com.climatechecker.domains;

import com.climatechecker.rest.deserializers.TemperatureDeserializer;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "temperature")
@NoArgsConstructor
@AllArgsConstructor
@JsonDeserialize(using = TemperatureDeserializer.class)
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Temperature {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "min_celsius")
    private Double min_celsius;

    @Column(name = "max_celsius")
    private Double max_celsius;

    @Column(name = "min_fahrenheit")
    private Double min_fahrenheit;

    @Column(name = "max_fahrenheit")
    private Double max_fahrenheit;

    @Column(name = "humidity")
    private Double humidity;

    @Column(name = "weather")
    private String weather;

    @Column(name = "tempdate")
    private LocalDate tempdate;

    @Column(name = "city_id")
    private Long city_id;
}
