package com.climatechecker.domains;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CitySummary {

    private Double TodayMax;
    private Double TodayMin;
    private Double oneyearMax;
    private Double oneyearMin;
    private Double fiveyearsMax;
    private Double fiveyearsMin;
    private Double tenyearsMax;
    private Double tenyearsMin;
    private Double twentyyearsMax;
    private Double twentyyearsMin;
    private Double fortyyearsMax;
    private Double fortyyearsMin;
}
