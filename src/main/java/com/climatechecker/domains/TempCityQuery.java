package com.climatechecker.domains;

import com.climatechecker.rest.deserializers.TempCityQueryDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@JsonDeserialize(using = TempCityQueryDeserializer.class)
public class TempCityQuery {
    private Long cityId;
    private LocalDate searchdate;

    public TempCityQuery(Long cityId, LocalDate searchdate) {
        this.cityId = cityId;
        this.searchdate = searchdate;
    }

}
