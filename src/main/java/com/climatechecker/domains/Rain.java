package com.climatechecker.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rain {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "1h", required = false)
    private Double rain1h;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "3h", required = false)
    private Double rain3h;
}
