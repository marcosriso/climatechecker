package com.climatechecker.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {

    private Long id;
    private ElementMainApiResponse main;
    private Coord coord;
    private List<Weather> weather;
    private String base;
    private Integer visibility;
    private Wind wind;
    private Clouds clouds;
    private Sys sys;
    private Integer timezone;
    private String name;
    private Integer cod;
    private Long dt;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Rain rain;

    @JsonProperty("dt_iso")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String dtiso;

    @JsonIgnore
    private String city_name;

    @JsonIgnore
    private String lat;

    @JsonIgnore
    private String lon;

}
