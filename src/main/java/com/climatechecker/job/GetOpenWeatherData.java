package com.climatechecker.job;

import com.climatechecker.domains.ApiResponse;
import com.climatechecker.domains.Temperature;
import com.climatechecker.rest.services.TemperatureService;
import com.climatechecker.utils.CelsiusToFahrenheitConverter;
import org.jboss.resteasy.plugins.providers.jackson.ResteasyJacksonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@EnableScheduling
public class GetOpenWeatherData {

    @Autowired
    TemperatureService temperatureService;

    @Scheduled(cron = "0 0 12 * * *" ) // Every noon
    public void GetOpenWeatherDataSchedule() throws Exception {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate now = LocalDate.now();

        String apiKey = "0fe5591f7b1b7b154098fdb859ee13fe";

        List<Temperature> templist = temperatureService.findByDate(now);

        /*
        templist.forEach(temperature -> {
            System.out.println(temperature.getTempdate());
        }); */

        if(templist.isEmpty()) {
            Client client = ClientBuilder.newBuilder().register(ResteasyJacksonProvider.class).build();
            WebTarget target = client.target("https://api.openweathermap.org/data/2.5/weather")
                    .queryParam("id", "3448439")
                    .queryParam("appid", apiKey)
                    .queryParam("units", "metric");

            ApiResponse response = target.request(MediaType.APPLICATION_JSON).get(ApiResponse.class);

            Temperature temperature = new Temperature();

            temperature.setCity_id(response.getId());
            temperature.setHumidity(response.getMain().getHumidity());
            temperature.setMin_celsius(response.getMain().getTemp_min());
            temperature.setMax_celsius(response.getMain().getTemp_max());
            temperature.setTempdate(now);
            temperature.setMin_fahrenheit(CelsiusToFahrenheitConverter.ConvertCToF(response.getMain().getTemp_min()));
            temperature.setMax_fahrenheit(CelsiusToFahrenheitConverter.ConvertCToF(response.getMain().getTemp_max()));
            temperature.setWeather(response.getWeather().get(0).getDescription());

            temperatureService.save(temperature);
        }

    }


}
