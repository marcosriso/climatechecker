package com.climatechecker;

import com.climatechecker.domains.ApiResponse;
import com.climatechecker.domains.Temperature;
import com.climatechecker.rest.services.TemperatureService;
import com.climatechecker.utils.CelsiusToFahrenheitConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.ws.rs.client.ClientBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "com.climatechecker.*" })
@ComponentScan("com.climatechecker.*")
@EntityScan("com.climatechecker.*")
public class Main {

    public static void main(String[] args){

        SpringApplication.run(Main.class, args);
        System.setProperty(ClientBuilder.JAXRS_DEFAULT_CLIENT_BUILDER_PROPERTY, "org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder");
    }

    /*
    @Bean
    CommandLineRunner runner(TemperatureService temperatureService) {
        return args -> {
            // read json and write to db
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<List<ApiResponse>> typeReference = new TypeReference<List<ApiResponse>>(){};
            InputStream inputStream = TypeReference.class.getResourceAsStream("/json/saopaulo.json");
            try {
                List<ApiResponse> apiresponse = mapper.readValue(inputStream,typeReference);
                apiresponse.forEach(apiResponse -> {
                    Temperature temperature = new Temperature();

                    String time = apiResponse.getDtiso().substring(11,19);
                    if(time.equalsIgnoreCase("12:00:00")) {
                        LocalDate date = LocalDate.parse(apiResponse.getDtiso().substring(0, 10));

                        temperature.setCity_id(apiResponse.getId());
                        temperature.setHumidity(apiResponse.getMain().getHumidity());
                        temperature.setMin_celsius(apiResponse.getMain().getTemp_min());
                        temperature.setMax_celsius(apiResponse.getMain().getTemp_max());
                        temperature.setTempdate(date);
                        temperature.setMin_fahrenheit(CelsiusToFahrenheitConverter.ConvertCToF(apiResponse.getMain().getTemp_min()));
                        temperature.setMax_fahrenheit(CelsiusToFahrenheitConverter.ConvertCToF(apiResponse.getMain().getTemp_max()));
                        temperature.setWeather(apiResponse.getWeather().get(0).getDescription());
                        temperature.setCity_id(Long.valueOf("3448439"));

                        temperatureService.save(temperature);
                    }
                });
            } catch (IOException e){
                System.out.println("Unable to save temperatures: " + e.getMessage());
            }
        };
    }*/

}

