package com.climatechecker.utils;

public class CelsiusToFahrenheitConverter {
    public static double ConvertCToF(double c) {
        // Convert Celsius to Fahrenheit.
        return ((9.0 / 5.0) * c) + 32;
    }
}
